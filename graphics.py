#imports útiles
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import itertools
import warnings
import plotly.express as px
from statistics import mean

# graphviz
#import graphviz
#from sklearn.tree import export_graphviz

# -------------------------------------------------------------     FUNCIONES -----------------------------------------------------------

# Se define una función para realizar los diferentes gráficos de serie de tiempo
def time_graph (x1,y1, title, label_x,pass_fail):
    
    if pass_fail:
        fig, ax = plt.subplots(figsize=(10, 8))

        ax.plot(x1, y1,'C2')
        ax.set_xlabel(label_x, fontdict= {'color': 'red', 'size': 15})
        ax.set_ylabel('Executions', fontdict= {'color': 'red', 'size': 15})

        plt.title(title, fontdict= {'color': 'red', 'size': 25})
  
    else:
        plt.plot(x1,y1,label='Pass')
        plt.plot(x1,y2,label='Fail')
        plt.title(title)
        plt.ylabel('Executions',fontdict= {'color': 'red', 'size': 15})
        plt.xlabel(label_x,fontdict= {'color': 'red', 'size': 15})
        plt.legend()

    return plt.show()

# Se define una función para crear nuava base de datos para análisis de variables categóricas
def new_data (column,data):
    
    newdata = data[[column, 'execution_status','bugs_found']].copy()

    return newdata


#Se define una función para determinar el top 5
def top_values (column,data,top):

    order = data.groupby(column).count().apply(lambda x: x.sort_values(ascending=False)) #ordenamos de forma descendente

    return print(order.iloc[:top])

#Se define una función para realizar los diferentes gráficos de variables con más de 5 valores
def max_graph (top,column,column_group,kind_grap,bugs):
    
    if bugs == False:
        title_comun = 'Porcentaje de ejecuciones fallidas/superadas según'
    else:
        title_comun = 'Porcentaje de errores según'
    
    title_specific = column
    title = title_comun + ' ' + title_specific
    
    if kind_grap=='bar':
        title = 'Gráfico barras'+ ' ' + title_specific
        pd.crosstab(index=top[column],
                columns =top[column_group],margins=False).apply(lambda r: r/r.sum()*100,
                                                         axis=0).plot(kind='bar',stacked=True,
                                                        title = title)
    else:
        if kind_grap=='pie':
            pd.crosstab(index=top[column],
                columns =top[column_group],margins=False).apply(lambda r: r/r.sum()*100,
                                                         axis=0).plot(kind='pie',subplots=True,
                                                                      autopct="%.2f",fontsize=10,
                                     figsize=(20,20),
                                     title = title)
        else:
            pd.crosstab(index=top[column_group],
                columns =top[column],margins=False).apply(lambda r: r/r.sum()*100,
                                                         axis=0).plot(kind='pie',subplots=True,
                                                                      autopct="%.2f",fontsize=10,
                                     figsize=(20,20),
                                     title = title)

    return pd.crosstab


#Se define función para realizar gráfico según las aplicaciones
def max_graph_app (dataset,column_group,kind_grap,bugs):
    
    if bugs == False:
        title_comun = 'Porcentaje de ejecuciones fallidas/superadas según app' 
    else:
        title_comun = 'Porcentaje de errores'
    
  
    if kind_grap=='bar':
        title = 'Gráfico barras'+ ' ' + title_comun 
        pd.crosstab(index=dataset['apps'],
                columns =dataset[column_group],margins=False).apply(lambda r: r/r.sum()*100,
                                                         axis=0).plot(kind='bar',stacked=True,
                                                        title = title_comun)
    else:
        pd.crosstab(index=dataset[column_group],
                columns =dataset['apps'],margins=False).apply(lambda r: r/r.sum()*100,
                                                         axis=0).plot(kind='pie',subplots=True,
                                                                      autopct="%.2f",fontsize=10,
                                     figsize=(10,10),
                                     title = title_comun)

    return pd.crosstab

#Se define función para estadísticas de palabras
def search_word (word):
       
    filter_search = data[data['test_name_stemmed'].str.match(word, case=False )]   
    
    return filter_search

def search_word_data (word):
    
    #Se busca en el dateframe que contiene las estadísticas, la columna con dicha palabra
    data_mask = new_data_words["word"] == word
    filtered_data = new_data_words[data_mask]
    filtered_data
    
    return filtered_data

#Se define función para graficar los resultados de los diferentes approach
def results_graph (precision_recall, data,approach,report = False):

    label_x = 'model'
    
    #construcción del gráfico
    if report == False:
        
        plt.style.use('ggplot')
        fig, ax = plt.subplots(figsize = (8, 6))
        
        title = 'Métrica' + ' ' + precision_recall + ' ' + 'Approach ' + approach
        
        label_y = 'mean_train_' + precision_recall

        plt.title(title, fontdict= {'color': 'green', 'size': 20})
        
        if precision_recall != 'precision_recall' and precision_recall != '':
            
            ax.set_xlabel('modelo', fontdict= {'color': 'green', 'size': 15})
            ax.set_ylabel(precision_recall, fontdict= {'color': 'green', 'size': 15})
        
            sns.scatterplot(data=data, x=label_x,y=label_y)
            
            #Guardar imagen
            image = precision_recall + "_" + approach + ".png"
            #save_image(image,plt)
        
            return sns.scatterplot
    
        elif precision_recall == 'precision_recall':
            
            ax.set_xlabel('precision', fontdict= {'color': 'green', 'size': 15})
            ax.set_ylabel('recall', fontdict= {'color': 'green', 'size': 15}) 

            sns.scatterplot(data=data, x='mean_train_precision',y='mean_train_recall',hue='model',ax=ax,
                 marker="$\circ$", ec="face", s=100,linewidth = 1.5)
            
            #Guardar imagen
            image = precision_recall + "_" + approach + ".png"
            #save_image(image,plt)
            
            return sns.scatterplot
    
    else:
        
        sns.relplot(data=data, x=label_x,y='mean',
           col='type',hue='type',kind='scatter')
        
        #Guardar imagen
        image = "reporte"+"_"+approach + ".png"
        #save_image(image,plt)
        
        return sns.relplot   

#Se define función para guardar los gráficos como imágenes
def save_image(name_image,graphic):
    
    path = 'C:/Users/agosm/Documents/tp_final/imagenes/'
    image = path+name_image
    
    #Guardar gráfico como imagen
    plt.savefig(image, format='png',dpi=300, bbox_inches = 'tight')
 
    return

#Se define función para graficar los resultados de los modelos
def evaluate_model(model, set_names=('train_structured', 'validation_structured'), title='', show_cm=False):
    if title:
        display(title)

    final_metrics = {
        'Accuracy': [],
        'Precision': [],
        'Recall': [],
        'F1': [],
    }

    for i, set_name in enumerate(set_names):
        assert set_name in ['train_structured', 'validation_structured', 'test_structured']
        set_data = globals()[set_name]

        y = set_data.execution_status
        y_pred = model.predict(set_data)
        final_metrics['Accuracy'].append(metrics.accuracy_score(y, y_pred))
        final_metrics['Precision'].append(metrics.precision_score(y, y_pred))
        final_metrics['Recall'].append(metrics.recall_score(y, y_pred))
        final_metrics['F1'].append(metrics.f1_score(y, y_pred))

        if show_cm:
            cm = metrics.confusion_matrix(y, y_pred)
            cm_plot = metrics.ConfusionMatrixDisplay(confusion_matrix=cm,
                                                     display_labels=['pass', 'fail'])
            cm_plot.plot(cmap="Oranges")

    display(pd.DataFrame(final_metrics, index=set_names))
    
#Se define función para graficar resultado de modelos de árbol
def graph_tree(tree, col_names):
    graph_data = export_graphviz(
        tree,
        out_file=None,
        feature_names=col_names,
        class_names=['pass', 'fail'],
        filled=True,
        rounded=True,
        special_characters=True,
    )
    graph = graphviz.Source(graph_data)
    return graph


def main():
    return
    

if __name__ == '__main__':
    main()