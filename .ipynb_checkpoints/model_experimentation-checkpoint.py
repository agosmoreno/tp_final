# Online Python - IDE, Editor, Compiler, Interpreter
import pandas as pd

import sklearn
from sklearn.feature_extraction.text import TfidfVectorizer, TfidfTransformer
from sklearn import metrics
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_validate 
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn_pandas import DataFrameMapper
from sklearn.preprocessing import minmax_scale

#Random fostest
from sklearn.ensemble import RandomForestClassifier

#Modelo Regresión Logística
from sklearn.linear_model import LogisticRegression

#Cross validation
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold

#Modelo RED Neuronal

#Modelo SVM
from sklearn.svm import LinearSVC

#Modelo MultinomialNB
from sklearn.naive_bayes import MultinomialNB


def dataset_creation(data,data_type):
    if data_type == 's':
        data.drop("executed_by",axis = 1,inplace = True)
        data.drop("creation_date",axis = 1,inplace = True)
        data.drop("test_name_clean",axis = 1,inplace = True)
        data.drop("test_name_stemmed",axis = 1,inplace = True)
        data.drop("test_name_lemmatized",axis = 1,inplace = True)
    elif data_type == 'ns':
        data.drop("cycle_name",axis = 1,inplace = True)
        data.drop("component",axis = 1,inplace = True)
        data.drop("version",axis = 1,inplace = True)
        data.drop("priority",axis = 1,inplace = True)
        data.drop("executed_by",axis = 1,inplace = True)
        data.drop("creation_date",axis = 1,inplace = True)
        data.drop("bugs_found",axis = 1,inplace = True)
        data.drop("apps",axis = 1,inplace = True)
    else:
        data.drop("executed_by",axis = 1,inplace = True) 
        data.drop("creation_date",axis = 1,inplace = True)
        
    return (data)

def test_train_validation(data):
    #stop = list(stopwords.words('spanish'))

    tfidf_vectorizer = TfidfVectorizer (sublinear_tf = True,min_df = 5, binary=False, 
                                    ngram_range=(1, 2),analyzer='word',token_pattern=r'\b[^\W\d_]+\b'
                                    ,max_features=5000) #,stop_words='spanish'

    features_tfidf = tfidf_vectorizer.fit_transform(data.test_name_lemmatized)

    predict_labels = data.execution_status
    
    # Separar los datos en train y test
    x_train, x_test, y_train, y_test,i_train, i_test = train_test_split(features_tfidf,
                                                predict_labels,data.index, test_size = 0.2, random_state =0)
        
    return (x_train, x_test, y_train, y_test,i_train, i_test)
    
def cross_validation_model(model,x_train,y_train):
    
    scoring = ['precision','recall','f1']
    folds = KFold(n_splits=5,shuffle  = True, random_state = 100)
    scores = cross_validate(model,x_train,y_train, 
        cv=folds,scoring=scoring,return_train_score=True)
    
    mean_test_precision = scores['test_precision'].mean() 
    mean_train_precision = scores['train_precision'].mean() 
    mean_test_recall = scores['test_recall'].mean() 
    mean_train_recall = scores['train_recall'].mean() 
    mean_test_f1 = scores['test_f1'].mean() 
    mean_train_f1 = scores['train_f1'].mean() 

    means_train_test = {'mean_test_precision': mean_test_precision,
                    'mean_train_precision': mean_train_precision,
                    'mean_test_recall': mean_test_recall,
                    'mean_train_recall': mean_train_recall,
                    'mean_test_f1': mean_test_f1,
                    'mean_train_f1': mean_train_f1}

    return(means_train_test)

def predic_model_no_structured(data,model='',data_type=''):
    
    no_structured = dataset_creation(data,data_type)
    
    x_train, x_test, y_train, y_test,i_train, i_test = test_train_validation(no_structured)
    
    if model == 'LR':
        
        model_predic  = LogisticRegression(C=12,max_iter=500)

        #model_predic.fit(x_train, y_train)

        #model_predic = model_predic.predict(x_test)
        
        #metrics = metrics.classification_report(y_test,model_predic)
        
        cv_model = cross_validation_model(model_predic,x_train,y_train)
        
        return (cv_model)
    
    elif model == 'SVM':
        
        model_predic = LinearSVC()
        
        #model_predic.fit(x_train, y_train)

        #model_predic = model_predic.predict(x_test)
        
        #metrics = metrics.classification_report(y_test,y_pred)
        
        cv_model = cross_validation_model(model_predic,x_train,y_train)
        
        return (cv_model)
        
    elif model == 'FOREST':
        model_predic = RandomForestClassifier(n_estimators=200,max_depth=3,random_state=0)

        #model_predic.fit(x_train, y_train)

        #model_predic = model_predic.predict_proba(x_test)

        cv_model = cross_validation_model(model_predic,x_train,y_train)
        
        return (cv_model)
        
    else:
        
        model_predic = MultinomialNB()

        #model_predic.fit(x_train, y_train)

        #model_predic = model_predic.predict_proba(x_test)

        cv_model = cross_validation_model(model_predic,x_train,y_train)
        
        return (cv_model)


def main():
    path=  'C:/Users/agosm/Documents/tp_final/'
    data = pd.read_csv(path + 'procesing_data.csv',skipfooter=0, engine='python')
    model= 'LR'
    data_type = 'ns'
    predic = predic_model_no_structured(data,model,data_type)
    
    with open('results_text_mean.txt',mode='w') as file_object:
        print(model,predic, file =file_object )
    
    print(model,predic)
    

if __name__ == '__main__':
    main()