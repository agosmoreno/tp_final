
from nltk import data
import pandas as pd

import sklearn
from sklearn import metrics
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_validate 
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn_pandas import DataFrameMapper
from sklearn.preprocessing import minmax_scale

# One hot encoding
from sklearn.preprocessing import OneHotEncoder

#Modelo Regresión Logística
from sklearn.linear_model import LogisticRegression
from sklearn.impute import SimpleImputer
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from sklearn.preprocessing import PolynomialFeatures

#Cross validation
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold

#Modelo Árbol
from sklearn.tree import DecisionTreeClassifier
from sklearn.preprocessing import LabelBinarizer

#Modelo Gradient Booosting
from sklearn.ensemble import GradientBoostingClassifier

#Modelo RED Neuronal

#Modelo SVM
from sklearn.svm import SVC

def dataset_creation(data,data_type):
    if data_type == 's':
        data.drop("executed_by",axis = 1,inplace = True)
        data.drop("creation_date",axis = 1,inplace = True)
        data.drop("test_name_clean",axis = 1,inplace = True)
        data.drop("test_name_stemmed",axis = 1,inplace = True)
        data.drop("test_name_lemmatized",axis = 1,inplace = True)
    elif data_type == 'ns':
        data.drop("cycle_name",axis = 1,inplace = True)
        data.drop("component",axis = 1,inplace = True)
        data.drop("version",axis = 1,inplace = True)
        data.drop("priority",axis = 1,inplace = True)
        data.drop("executed_by",axis = 1,inplace = True)
        data.drop("creation_date",axis = 1,inplace = True)
        data.drop("bugs_found",axis = 1,inplace = True)
        data.drop("apps",axis = 1,inplace = True)
    else:
        data.drop("executed_by",axis = 1,inplace = True) 
        data.drop("creation_date",axis = 1,inplace = True)
        
    return (data)

def test_train_validation(data):
    
    data.sort_values('executed_on',ascending = False)
    
    test = data[data.executed_on.between('2022-09-05', 
                           '2023-12-09')]
    data_structured = data[data.executed_on < '2022-09-05']
    validation = data_structured[data_structured.executed_on.between('2021-12-21', 
                           '2022-09-05')]
    train = data_structured[data_structured.executed_on < '2021-12-21']
        
    return (test,train,validation)
    
def cross_validation_model(model,data):
    
    scoring = ['precision','recall','f1']
    folds = KFold(n_splits=5,shuffle  = True, random_state = 100)
    scores = cross_validate(model, data, data.execution_status, 
        cv=folds,scoring=scoring,return_train_score=True)
    
    mean_test_precision = scores['test_precision'].mean() 
    mean_train_precision = scores['train_precision'].mean() 
    mean_test_recall = scores['test_recall'].mean() 
    mean_train_recall = scores['train_recall'].mean() 
    mean_test_f1 = scores['test_f1'].mean() 
    mean_train_f1 = scores['train_f1'].mean() 

    means_train_test = {'mean_test_precision': mean_test_precision,
                    'mean_train_precision': mean_train_precision,
                    'mean_test_recall': mean_test_recall,
                    'mean_train_recall': mean_train_recall,
                    'mean_test_f1': mean_test_f1,
                    'mean_train_f1': mean_train_f1}

    return(means_train_test)

def predic_model_structured(data,model='',data_type=''):
    
    structured = dataset_creation(data,data_type)
    
    test_structured, train_structured, validation_structured = test_train_validation(structured)
    
    complete_mapper = DataFrameMapper([
    (['cycle_name'], [OneHotEncoder(handle_unknown='ignore')]), 
    (['component'], [OneHotEncoder(handle_unknown='ignore')]), 
    (['version'], [OneHotEncoder(handle_unknown='ignore')]), 
    (['priority'], [StandardScaler()]),
    (['apps'], [OneHotEncoder(handle_unknown='ignore')]),
    ])
    
    #complete_mapper.fit(train_structured)
    
    if model == 'LR':
        
        model_predic  = LogisticRegression(C=12,max_iter=300)
        
        model_predic = Pipeline([('mapper', complete_mapper),
                        ('imputer', SimpleImputer(strategy='mean')),
                        ('classifier', LogisticRegression(random_state=42)),
                        ])

        #model_predic.fit(train_structured, train_structured.execution_status)

        #model_predic = model_predic.predict(validation_structured)
        
        cv_model_train = cross_validation_model(model_predic,train_structured)
        
        cv_model_val = cross_validation_model(model_predic,validation_structured)
        
        return (cv_model_train,cv_model_val)
    
    elif model == 'LRII':
        model_predic  = LogisticRegression(C=12,max_iter=00)
        
        model_predic = Pipeline([('mapper', complete_mapper),
                                ('imputer', IterativeImputer(random_state=42)),
                                ('classifier', LogisticRegression(random_state=42))])
        
        #model_predic.fit(train_structured, train_structured.execution_status)

        #model_predic = model_predic.predict(validation_structured)
        
        cv_model = cross_validation_model(model_predic,train_structured)
        
        return (cv_model)
        
    elif model == 'LRP':
        model_predic  = LogisticRegression(C=12,max_iter=300)
        
        model_predic = Pipeline([('mapper', complete_mapper),
                                ('imputer', IterativeImputer(random_state=42)),
                                ('poli', PolynomialFeatures(degree=2)),
                                ('classifier', LogisticRegression(max_iter=3000, random_state=42)),])

        #model_predic.fit(train_structured, train_structured.execution_status)
        
        #model_predic = model_predic.predict(validation_structured)
        
        cv_model = cross_validation_model(model_predic,train_structured)
        
        return (cv_model)  
        
    elif model == 'TREE':
        model_predic = DecisionTreeClassifier(random_state=42,max_depth=3)

        model_predic = Pipeline([('mapper', complete_mapper),
                                ('imputer', IterativeImputer(random_state=42)),
                                ('classifier', model_predic),
                                ])

        #model_predic.fit(train_structured, train_structured.execution_status)
        #a = model_predic.steps[2][1]
        #b = model_predic.steps[0][1]
        cv_model = cross_validation_model(model_predic,train_structured)
        
        return (cv_model)
        
    elif model == 'BOOST':
        
        model_predic = GradientBoostingClassifier(random_state=42)
        model_predic = Pipeline([('mapper', complete_mapper),
                        ('imputer', IterativeImputer(random_state=42)),
                        ('classifier', model_predic),
                        ])       
        
        #model_predic.fit(train_structured, train_structured.execution_status)
        #model_predic = model_predic.predict_proba(validation_structured)
        
        cv_model = cross_validation_model(model_predic,train_structured)
        
        return (cv_model)
        
    elif model == 'SVM':
        
        clf = SVC(kernel='linear',C = 1.0)
        
        cv_model = cross_validation_model(clf,train_structured)
        
        return (cv_model)
    else:
        return ()


def main():
    path=  'C:/Users/agosm/Documents/tp_final/'
    data = pd.read_csv(path + 'procesing_data.csv',skipfooter=0, engine='python')
    model= 'LR'
    data_type = 's'
    predic = predic_model_structured(data,model,data_type) #devolver modelo entrenado?
    
    with open('results_mean.txt',mode='w') as file_object:
        print(model,predic, file =file_object ) #sobreescibe datos
    
    print(model,predic)

if __name__ == '__main__':
    main()
