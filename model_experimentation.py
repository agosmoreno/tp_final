# IMPORTS ÚTILES
import pandas as pd
import numpy as np
import re

import sklearn
from sklearn.feature_extraction.text import TfidfVectorizer, TfidfTransformer
from sklearn import metrics
from sklearn.metrics import make_scorer,accuracy_score, precision_score, recall_score, f1_score, confusion_matrix, classification_report
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_validate 
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn_pandas import DataFrameMapper
from sklearn.preprocessing import minmax_scale

#Cross validation
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold

# One hot encoding
from sklearn.preprocessing import OneHotEncoder

#Random fostest
from sklearn.ensemble import RandomForestClassifier

#Modelo Regresión Logística
from sklearn.linear_model import LogisticRegression
from sklearn.impute import SimpleImputer
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from sklearn.preprocessing import PolynomialFeatures

#Modelo Árbol
from sklearn.tree import DecisionTreeClassifier
from sklearn.preprocessing import LabelBinarizer

#Modelo Gradient Booosting
from sklearn.ensemble import GradientBoostingClassifier

#Modelo RED Neuronal

#Modelo SVM
from sklearn.svm import LinearSVC
#from sklearn.pipeline import make_pipeline
#from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC

#Modelo MultinomialNB
from sklearn.naive_bayes import MultinomialNB

#Modelo Ensemble
from sklearn.ensemble import VotingClassifier, StackingClassifier
from sklearn.model_selection import GridSearchCV

# -------------------------------------------------------------     FUNCIONES -----------------------------------------------------------

#Se define función para obtener dataset según tipo de experimentación
def dataset_creation(data,data_type):
    
    copy_data = data.copy()
    
    if data_type == 's':
        data.drop("executed_by",axis = 1,inplace = True)
        data.drop("creation_date",axis = 1,inplace = True)
        data.drop("test_name_clean",axis = 1,inplace = True)
        data.drop("test_name_stemmed",axis = 1,inplace = True)
        data.drop("test_name_lemmatized",axis = 1,inplace = True)
    elif data_type == 'ns':
        data.drop("cycle_name",axis = 1,inplace = True)
        data.drop("component",axis = 1,inplace = True)
        data.drop("version",axis = 1,inplace = True)
        data.drop("priority",axis = 1,inplace = True)
        data.drop("executed_by",axis = 1,inplace = True)
        data.drop("creation_date",axis = 1,inplace = True)
        data.drop("bugs_found",axis = 1,inplace = True)
        data.drop("apps",axis = 1,inplace = True)
    else:
        data.drop("executed_by",axis = 1,inplace = True) 
        data.drop("creation_date",axis = 1,inplace = True)
        
    return (data,copy_data)

#Se define función para dividir dataset 
def test_train(data,data_type):
    
    data.sort_values('executed_on',ascending = False)
    
    if data_type == 's': #división dataset para datos estructurados
        
        test = data[data.executed_on.between('2022-09-05', 
                           '2023-12-09')]
        
        data_structured = data[data.executed_on < '2022-09-05']
        
        train_validation = data_structured[data_structured.executed_on < '2022-09-05']
        
        return (test,train_validation)
    
    else: #división dataset para datos no estructurados
        
        tfidf_vectorizer = TfidfVectorizer (sublinear_tf = True,min_df = 5, binary=False, 
                                    ngram_range=(1, 2),analyzer='word',token_pattern=r'\b[^\W\d_]+\b'
                                    ,max_features=5000) #,stop_words='spanish'
        
        features_tfidf = tfidf_vectorizer.fit_transform(data.test_name_lemmatized)

        predict_labels = data.execution_status
        
        # Separar los datos en train y test
        x_train, x_test, y_train, y_test = train_test_split(features_tfidf,
                                                predict_labels,test_size = 0.2, random_state =0)
        
        return (x_train, x_test, y_train, y_test)


#Se define función de cross
def cross_validation_model(model,x_train,y_train):  
    
    scoring = ['precision','recall','f1']
    folds = KFold(n_splits=5,shuffle  = True, random_state = 100)
    
      
    scores = cross_validate(model,x_train,y_train, 
            cv=folds,scoring=scoring,return_train_score=True)
    
    
    #diccionario con la media de los resultados de las métricas    
    mean_validation_precision = scores['test_precision'].mean() 
    mean_train_precision = scores['train_precision'].mean() 
    mean_validation_recall = scores['test_recall'].mean() 
    mean_train_recall = scores['train_recall'].mean() 
    mean_validation_f1 = scores['test_f1'].mean() 
    mean_train_f1 = scores['train_f1'].mean() 

    means_train_test = {'mean_validation_precision': mean_validation_precision,
                    'mean_train_precision': mean_train_precision,
                    'mean_validation_recall': mean_validation_recall,
                    'mean_train_recall': mean_train_recall,
                    'mean_validation_f1': mean_validation_f1,
                    'mean_train_f1': mean_train_f1}

    return(means_train_test)

#Función para construir el mapper
def mapper_construction(data,approach):
        
        if approach == 1: #se define el DataFrame Mapper para los modelos con dataset estructurado
        
            complete_mapper = DataFrameMapper([
            (['cycle_name'], [OneHotEncoder(handle_unknown='ignore')]), 
            (['component'], [OneHotEncoder(handle_unknown='ignore')]), 
            (['version'], [OneHotEncoder(handle_unknown='ignore')]), 
            (['priority'], [StandardScaler()]),
            (['apps'], [OneHotEncoder(handle_unknown='ignore')]),
            ])
        
        
        else: #se define el DataFrame Mapper para los modelos con dataset estructurado y no estructurado
        
            tfidf_vectorizer = TfidfVectorizer (sublinear_tf = True,min_df = 5, binary=False, 
                                    ngram_range=(1, 2),analyzer='word',token_pattern=r'\b[^\W\d_]+\b'
                                    ,max_features=1000) #,stop_words='spanish5
            if approach == 4:
                
                complete_mapper = DataFrameMapper([('test_name_lemmatized',tfidf_vectorizer)])
                
            else:
                
                complete_mapper = DataFrameMapper([
                    (['cycle_name'], [OneHotEncoder(handle_unknown='ignore')]), 
                    (['component'], [OneHotEncoder(handle_unknown='ignore')]), 
                    (['version'], [OneHotEncoder(handle_unknown='ignore')]), 
                    (['priority'], [StandardScaler()]),
                    (['apps'], [OneHotEncoder(handle_unknown='ignore')]),
                    ('test_name_lemmatized',tfidf_vectorizer)])
            
        return complete_mapper

#Se define función para modelos estructurados Approach 1
def predic_model(train,model='',approach=int):
    
    complete_mapper = mapper_construction(train,approach)
   
    
    if model == 'LR':
        
        model_predic  = LogisticRegression(C=12,max_iter=300)
        
        model_predic = Pipeline([('mapper', complete_mapper),
                        ('imputer', SimpleImputer(strategy='mean')),
                        ('classifier', LogisticRegression(random_state=42)),
                        ])
      
        cv_model = cross_validation_model(model_predic,train,train.execution_status)
                     
        return (cv_model)
    
    elif model == 'LRII':
        model_predic  = LogisticRegression(C=12,max_iter=300)
        
        model_predic = Pipeline([('mapper', complete_mapper),
                                ('imputer', IterativeImputer(random_state=42)),
                                ('classifier', LogisticRegression(random_state=42))])
     
    
        cv_model = cross_validation_model(model_predic,train,train.execution_status)
        
        return (cv_model)
        
    elif model == 'LRP':
        model_predic  = LogisticRegression(C=12,max_iter=300)
        
        model_predic = Pipeline([('mapper', complete_mapper),
                                ('imputer', IterativeImputer(random_state=42)),
                                ('poli', PolynomialFeatures(degree=2)),
                                ('classifier', LogisticRegression(max_iter=3000, random_state=42)),])

        
        cv_model = cross_validation_model(model_predic,train,train.execution_status)
        
        return (cv_model)  
        
    elif model == 'TREE':
        model_predic = DecisionTreeClassifier(random_state=42,max_depth=3)

        model_predic = Pipeline([('mapper', complete_mapper),
                                ('imputer', IterativeImputer(random_state=42)),
                                ('classifier', model_predic),
                                ])

        #model_predic.fit(train_structured, train_structured.execution_status)
        #a = model_predic.steps[2][1]
        #b = model_predic.steps[0][1]
        cv_model = cross_validation_model(model_predic,train,train.execution_status)
        
        return (cv_model)
        
    elif model == 'BOOST':
        
        model_predic = GradientBoostingClassifier(random_state=42)
        model_predic = Pipeline([('mapper', complete_mapper),
                        ('imputer', IterativeImputer(random_state=42)),
                        ('classifier', model_predic),
                        ])       
        
        cv_model = cross_validation_model(model_predic,train,train.execution_status)
        
        return (cv_model)
        
    elif model == 'SVM':
        
        clf = SVC(kernel='linear',C = 1.0)
        
        clf = Pipeline([('mapper', complete_mapper),
                         ('imputer', SimpleImputer(strategy='mean')),
                         ('clf', clf),
                        ])  
        
        cv_model = cross_validation_model(clf,train,train.execution_status)
        
        return (cv_model)
    else:
        return ()

#Se define función para modelos no estructurados Approach 2
def predic_model_no_structured(x_train,y_train,model=''):
    
    if model == 'LR':
        
        model_predic  = LogisticRegression(C=12,max_iter=500)
    
        cv_model = cross_validation_model(model_predic,x_train,y_train)
        
        return (cv_model)
    
    elif model == 'SVM':
        
        model_predic = LinearSVC()
        
        cv_model = cross_validation_model(model_predic,x_train,y_train)
        
        return (cv_model)
        
    elif model == 'FOREST':
        
        model_predic = RandomForestClassifier(class_weight='balanced',n_estimators=200,max_depth=3,random_state=0) #balance  class_weight=dict_weights

        cv_model = cross_validation_model(model_predic,x_train,y_train)
        
        return (cv_model)
        
    else:
        
        model_predic = MultinomialNB()

        cv_model = cross_validation_model(model_predic,x_train,y_train)
        
        return (cv_model)
    
#Se define función para la creación de pipelines en el approach 4
def create_pipeline(mapper, imputer, classifier, poly_degree=None):
    steps = [('mapper', mapper)]
    
    if imputer:
        steps.append(('imputer', imputer))
    
    if poly_degree:
        steps.append(('poli', PolynomialFeatures(degree=poly_degree, include_bias=False)))
    
    steps.append(('classifier', classifier))
    
    return Pipeline(steps)

#Se define función para realizar una búsqueda de hiperparámetros (approach 4)
def perform_grid_search(pipeline, param_grid, train_data, target):
    
    scoring = {
        'precision': make_scorer(precision_score, average='macro'),
        'recall': make_scorer(recall_score, average='macro'),
        'f1': make_scorer(f1_score, average='macro')
    }
    
    grid_search = GridSearchCV(pipeline, param_grid, cv=5, scoring=scoring, refit='f1', n_jobs=-1)
    grid_search.fit(train_data, target)
    
    return grid_search.best_estimator_

#Se define función para modelo ensemble: Approach 4
def predic_model_ensemble(train, cant_models, model):
    
    complete_mapper_approach_1 = mapper_construction(train, 1)
    complete_mapper_approach_2 = mapper_construction(train, 4)

    # Pipelines
    pipeline_approach_1 = create_pipeline(complete_mapper_approach_1,
                                          IterativeImputer(random_state=42),
                                          LogisticRegression(max_iter=3000, random_state=42),
                                          poly_degree=2)

    pipeline_approach_2 = create_pipeline(complete_mapper_approach_2,None,LogisticRegression(class_weight='balanced', max_iter=5000))

    #Parámetros para GridSearchCV
    param_grid_approach_1 = {'imputer__max_iter': [10, 50, 100],
                             'poli__degree': [1, 2, 3],
                             'classifier__C': [0.1, 1, 10]}

    param_grid_approach_2 = {'classifier__C': [0.01, 0.1, 1, 10]}

    # Ajustar modelos
    best_model_approach_1 = perform_grid_search(pipeline_approach_1, param_grid_approach_1, train, train.execution_status)
    best_model_approach_2 = perform_grid_search(pipeline_approach_2, param_grid_approach_2, train, train.execution_status)
    
    # Crear instancias de GridSearchCV con diferentes métricas
    scoring = {'precision': make_scorer(precision_score, average='macro'),
               'recall': make_scorer(recall_score, average='macro'),
               'f1': make_scorer(f1_score, average='macro')}
    
    if cant_models == 2:
        
        param_grid_approach_1_a = {
            'imputer__max_iter': [10, 50, 100],  # Ejemplo de parámetros para IterativeImputer
            'classifier__n_estimators': [50, 100, 200]
        }

        param_grid_approach_2_a = {
            'imputer__strategy': ['mean', 'median'],
            'classifier__C': [0.01, 0.1, 1, 10]
        }
        
        pipeline_approach_1_a = Pipeline([
            ('mapper', complete_mapper_approach_1),
            ('imputer', IterativeImputer(random_state=42)),
            ('classifier', GradientBoostingClassifier(random_state=42))
            ])

        pipeline_approach_2_a = Pipeline([
            ('mapper', complete_mapper_approach_2),
            ('imputer', SimpleImputer(strategy='mean')),
            ('classifier', SVC(kernel="linear", probability=True))
            ])
        
        grid_search_approach_1_a = GridSearchCV(pipeline_approach_1_a, param_grid_approach_1_a, cv=5, scoring=scoring, refit='f1', n_jobs=-1)
        grid_search_approach_2_a = GridSearchCV(pipeline_approach_2_a, param_grid_approach_2_a, cv=5, scoring=scoring, refit='f1', n_jobs=-1)

        best_model_approach_1_a = perform_grid_search(pipeline_approach_1_a, param_grid_approach_1_a, train, train.execution_status)
        best_model_approach_2_a = perform_grid_search(pipeline_approach_2_a, param_grid_approach_2_a, train, train.execution_status)
        
        estimators = [
            ('lrp', best_model_approach_1),
            ('boost', best_model_approach_1_a),
            ('lr', best_model_approach_2),
            ('svm', best_model_approach_2_a)
           ]
    
    else:
        # Siempre incluir dos modelos en el VotingClassifier
        estimators = [
            ('lrp', best_model_approach_1),
            ('lr', best_model_approach_2)
        ]

    # Definir los pesos a probar
    weights_list = [np.arange(0.0, 1.1, 0.1) for _ in range(len(estimators))]
    
    param_grid_weights = { 'weights': [list(weights) for weights in np.array(np.meshgrid(*weights_list)).T.reshape(-1, len(estimators)) if np.sum(weights) == 1]}
    
   
    # Crear el VotingClassifier y buscar los mejores pesos
    voting_clf = VotingClassifier(estimators=estimators, voting='soft', n_jobs=-1)
    
    # Ajustar el modelo con los pesos 
    grid_search_weights = GridSearchCV(voting_clf, param_grid_weights, cv=5, scoring=scoring, refit='f1', n_jobs=-1)
    grid_search_weights.fit(train, train.execution_status)

    # Obtener el mejor modelo
    #best_voting_model = grid_search_weights.best_estimator_
    
    # Calcular y mostrar métricas en el conjunto de entrenamiento 
    #train_predictions = best_voting_model.predict(train)
    
    #print("Métricas del modelo en el conjunto de entrenamiento:")
    #print(classification_report(train.execution_status, train_predictions))
    
    # Acceder a los resultados
    cv_results = grid_search_weights.cv_results_
    
    return cv_results, grid_search_weights


#Se define función para guardar resultados de los modelos en un txt
def results_txt(model_name,result_predic,approach):
    
    if approach == 1:
    
        txt_name= 'results_structured_models_mean.txt'
        
    elif approach == 2:
        
        txt_name= 'results_no_structured_models_mean.txt'
        
    elif approach == 3:
        
        txt_name= 'results_both_models_mean.txt'
         
    else:
        
        txt_name= 'results_hibrido_models_mean.txt'
        
        
    with open(txt_name,mode='a') as file_object:
        print(model_name,result_predic, file =file_object )
    
    return

#Lectura de resultados
def read_results(txt=''):
    
    results = pd.read_table(txt, header=None, sep=" ")
    
    #Se renombran las columnas
    results.columns = ['model','1','mean_validation_precision','3','mean_train_precision','5','mean_validation_recall','7',
           'mean_train_recall','9','mean_validation_f1','11','mean_train_f1']

    #Se eliminan las columnas de texto
    results.drop("1",axis = 1,inplace = True)
    results.drop("3",axis = 1,inplace = True)
    results.drop("5",axis = 1,inplace = True)
    results.drop("7",axis = 1,inplace = True)
    results.drop("9",axis = 1,inplace = True)
    results.drop("11",axis = 1,inplace = True)

    #Eliminación de caracteres especiales
    results = results.replace(',','', regex=True)
    results = results.replace('}','', regex=True)

    #Tabla de resultados finales
    return results

# Función para leer datos del approach 4
def extraer_datos(ruta_archivo):
    
    # Leer el contenido del archivo
    with open(ruta_archivo, 'r') as file:
        texto = file.read()
    
    resultado = {}
    
    # Buscar arrays y extraer
    datos = re.findall(r'\'(\w+)\'\:\s*array\(\[([^\]]+)\]\)', texto)
    for key, values in datos:
        resultado[key] = np.array([float(x) for x in values.split(',')])

    # Extraer param_weights
    weights_match = re.search(r'\'param_weights\'\:\s*masked_array\(data=\[(.+?)\],', texto, re.DOTALL)
    params_match = re.search(r'\'params\'\:\s*\[(.*?)\]', texto, re.DOTALL)
    
    if weights_match:
        weights_str = weights_match.group(1)
        weights_list = re.findall(r'\[([0-9., ]+)\]', weights_str)
        resultado['param_weights'] = [list(map(float, w.split(','))) for w in weights_list]

    # Mostrar longitudes de los datos extraídos
    for key, value in resultado.items():
        print(f"{key}: {len(value)}")

    return resultado

def main():

    return
    

if __name__ == '__main__':
    main()