#imports útiles
import numpy as np
import pandas as pd
import ast
from collections import defaultdict

#import para datos
import nltk
from nltk.corpus import stopwords #eliminación stopwords
from nltk.tokenize import word_tokenize #contar palabras
from nltk.stem import WordNetLemmatizer
from nltk.stem.snowball import SnowballStemmer
from nltk.stem.porter import PorterStemmer
import re
from sklearn.feature_extraction.text import TfidfVectorizer, TfidfTransformer
from sklearn.metrics import make_scorer,accuracy_score, precision_score, recall_score, f1_score, confusion_matrix, classification_report

#limpieza texto
from nltk.corpus import stopwords

# -------------------------------------------------------------     FUNCIONES -----------------------------------------------------------

#Convertir columnas a tipo float 64
def convert_to_numeric (data,columns):
    
    data[columns] = data[columns].astype('float64')
    
    return data

#limpieza del dateset
def clean_dataset(data):
    
    #Completar columnas que tengan valores NaN con 0
    data.fillna(0, inplace=True)
    
    #Se agrega una nueva columna para determinar cantidad de bugs
    data['bugs_found'] = np.where(data['ExecutionDefects']>=1,"found_bugs","not_found_bugs")
    
    #Transformar columna fechas al formato correspondiente
    data = pd.DataFrame(data)
    data['CreationDate'] = pd.to_datetime(data['CreationDate'])
    data['Executed On'] = pd.to_datetime(data['Executed On'])
    
    data = mapper_values(data)
    
    #se eliminan la columna test_name y description_without_stopwords + las columnas que no se utilizarán
    data.drop("ExecutionId",axis = 1,inplace = True)
    data.drop("Project",axis = 1,inplace = True)
    data.drop("Interfaz",axis = 1,inplace = True) #se elimina ya que existen mucho mas datos null
    data.drop("Assigned To",axis = 1,inplace = True)
    data.drop(["ExecutionDefects"], axis=1, inplace=True)
    
    return (data)

#mapear los valores de las columnas categóricas a int/float
def mapper_values(data):
    
    # Mapear los valores de la columna priority 
    mapeo_priority = {'Bloqueadora':5, 'Crítica':4,'Mayor':3,'Media':2,'0':2,  'Menor':1,'Trivial':0}
    data['Priority'] = data.loc[:, 'Priority'].map(mapeo_priority)
    
    # Mapear los valores de la columna ExecutionStatus (dos variables)
    mapeo_status = {'SUPERADO':'pass', 'NO SUPERADO':'fail','WIP':'fail','BLOQUEADA':'fail',' NO SUPERADO':'fail',  'DIFERIDO':'fail'}
    data['Execution Status'] = data.loc[:, 'Execution Status'].map(mapeo_status) 
    
    return (data)

#preprocesado del texto
def pre_processing_text(data):
    
    #se eliminan las stopwords
    stop = stopwords.words('spanish')
    
    data['description_without_stopwords'] = data['Test Summary'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stop)]))


    # Eliminacion de signos
    import string
    PUNCT_TO_REMOVE = string.punctuation

    def remove_punctuation(text):
        """custom function to remove the punctuation"""
        return text.translate(str.maketrans('', '', PUNCT_TO_REMOVE))

    data["description_without_stopwords"] = data["description_without_stopwords"].apply(lambda text: remove_punctuation(text))
    
    #se eliminan la columna test_name + las columnas que no se utilizarán
    data.drop(["Test Summary"], axis=1, inplace=True)
    data.drop(["Issue Key"], axis=1, inplace=True)
        
    
    return (data)

#función para renombrar las columnas según notebook (si es EDA habrá una columna de más utilizada para análisis
def rename_column(data,notebook=''):
    
    if notebook == 'eda':
        data.columns=['cycle_name','issue_key', 'test_name','component','version',
              'priority','executed_by','executed_on', 'execution_status', 'creation_date','bugs_found']
    else:
        data.columns=['cycle_name','component','version','priority','executed_by','executed_on',
              'execution_status', 'creation_date','bugs_found','test_name_clean']
    
    return data

        
#stemming y lemmatization del texto
def stemming_lemmatization(data):
    
    #dataset = clean_dataset(data)
    #dataset = pre_processing_text(dataset)
    
    
    # Stemming
    stemmer = PorterStemmer()

    def stem_words(text):
        return " ".join([stemmer.stem(word) for word in text.split()])

    data["test_name_stemmed"] = data["test_name_clean"].apply(lambda text: stem_words(text))

    stemmer  = SnowballStemmer(language='spanish') #setup for spanish language
    
    
    # Lemmatization
    lemmatizer = WordNetLemmatizer()

    def lemmatize_words(text):
        return " ".join([lemmatizer.lemmatize(word) for word in text.split()])

    data["test_name_lemmatized"] = data["test_name_clean"].apply(lambda text: lemmatize_words(text))

    return (data)

#nueva columna que devuelve la aplicación que se utiliza en el caso (gw,nova,time,bup,sap,ceibo
def new_column_app(app,data):
    
    data[app] = data.test_name_stemmed.str.startswith(app)

    #se define la condición para completar con el valor de la nueva app
    conditions = [(data[app] == True),(data[app] == False)]

    #se define el valor a completar
    choices = [app,data['apps']] #si el caso tiene en el nombre la aplicación se completará con la misma, en caso contrario el valor será el que ya tien

    #se completa la columna
    data['apps'] = np.select(conditions, choices, default='')
    
    #se elimina la columna generada correspondiente a la aplicación 
    data.drop(app,axis = 1,inplace = True)


    return data

#Función para el dataset de los resultados en el escenario 4
def clean_results_4(data,columns):

    # Supongamos que quieres conservar 'columna1', 'columna2' y 'columna3'
    columnas_a_eliminar = [col for col in data.columns if col not in columns]

    # Eliminar las columnas no deseadas
    df_reducido = data.drop(columns=columnas_a_eliminar)
    
    df_reducido.columns=['mean_train_precision','mean_train_recall','mean_train_f1','model']
    
    #df_reducido['model'] = df_reducido['model'].apply(lambda x: f"[{x[0]}, {x[1]}]")

    # Asegurarse de que todos los elementos sean listas
    df_reducido['model'] = df_reducido['model'].apply(lambda x: ast.literal_eval(x) if isinstance(x, str) else x)

    # Modificar la columna 'model'
    df_reducido['model'] = df_reducido['model'].apply(lambda x: f"[{round(x[0], 1)}, {round(x[1], 1)}]")

    
    return df_reducido

#Función para crear un nuevo dataset con los resultados de las experimentaciones y compararlas
def metrics_comparative (data, graphic,columns,best_model):
    
    if graphic:
        dfprecision = data.groupby(['model'])['mean_train_precision'].agg(['mean']).reset_index()
        dfprecision['type']= 'precision'

        #dfprecision_validation = results_approach_1.groupby(['model'])['mean_validation_precision'].agg(['mean']).reset_index()
        dfrecall = data.groupby(['model'])['mean_train_recall'].agg(['mean']).reset_index()
        dfrecall['type']= 'recall'

        df1_scores = data.groupby(['model'])['mean_train_f1'].agg(['mean']).reset_index()
        df1_scores['type']= 'f1'

        data = pd.concat([dfprecision,dfrecall,df1_scores])
        return data
        
    else:
            
     
        data = data[columns]
        
        if best_model:
            
            #best_model = ['LRP', 'LR', 'SVM', 'LR_5000_features']

            # Condiciones por tipo de 'approach'
            structured_models = ['LRP']
            non_structured_models =  ['LR', 'SVM']
            both_approach_models = ['LR_5000_features']
            ensemble = ['[0.3, 0.7]']

            # Filtrar datos
            data = data.loc[
                            (data['model'].isin(structured_models) & (data['approach'] == 'strucutured')) | 
                            (data['model'].isin(non_structured_models) & (data['approach'] == 'no_strucutured')) | 
                            (data['model'].isin(both_approach_models) & (data['approach'] == 'both')) |
                            (data['model'].isin(ensemble) & (data['approach'] == 'ensemble'))
                           ]

        return data
    
#Función para abreviacione en gráfico de resultados para escenario 4
def predict_tst_best_model(data,model):
    
    test_predictions = model.predict(data)
    test_predictions
    
    #diccionario con la media de los resultados de las métricas    
    mean_test_precision = precision_score(data.execution_status, test_predictions, average='macro').mean() 
    mean_test_recall = recall_score(data.execution_status, test_predictions, average='macro').mean() 
    mean_test_f1 = f1_score(data.execution_status, test_predictions, average='macro').mean() 

    means_test = {'mean_test_precision': mean_test_precision,
              'mean_test_recall': mean_test_recall,
              'mean_test_f1': mean_test_f1}

    report = classification_report(data.execution_status, test_predictions)
    
    return means_test, report
 
def main():
    return
    

if __name__ == '__main__':
    main()